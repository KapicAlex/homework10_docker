export const ROUTER = {
  homepage: "/",
  login: "/login",
  userList: "/user-list",
  chat: "/chat",
  userEditor: "/user-editor",
  messageEditor_$ID: "/message-editor/:id",
}