import { Http } from "./http/http.service";
import { Auth } from "./authentication/auth.service";
import { Message } from "./message/message.service";
import { User } from "./user/user.service";

const http = new Http();
const auth = new Auth({ http });
const message = new Message({ http });
const user = new User({ http });

export { http, auth, message, user };
