import { ContentType, HttpMethod } from '../../enums/http/http';

class Message {
  constructor({ http }) {
    this._http = http;
  }

  getAllMessages() {
    return this._http.load('/api/messages', {
      method: HttpMethod.GET
    });
  }

  getMessage(id) {
    return this._http.load(`/api/messages/${id}`, {
      method: HttpMethod.GET
    });
  }

  addMessage(payload) {
    return this._http.load('/api/messages', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  editMessage(payload) {
    return this._http.load('/api/messages', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  deleteMessage(id) {
    return this._http.load(`/api/messages/${id}`, {
      method: HttpMethod.DELETE
    });
  }
}

export { Message };