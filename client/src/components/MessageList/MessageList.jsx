import React from "react";
import moment from 'moment';
import { useSelector } from "react-redux";
import "./messageList.css";
import Message from "../Message/Message";
import OwnMesssage from "../OwnMessage/OwnMessage";
import Divider from "../Divider/Divider";

const MessageList = ({userId}) => {
  const messages = useSelector(state => state.chat.messages);

  return (
          <div className="message-list">
    
            {messages.map((message, idx) => {
              let temp = [];
              const prevDate = idx === 0 ? 1 : messages[idx-1].createdAt;
              const currentDate = message.createdAt;
    
              if(message.userId === userId) {
    
                if(prevDate && moment(prevDate).calendar() !== moment(currentDate).calendar()) {
                  temp.push(<Divider key={message.createdAt} date={message.createdAt} />);
                }
                temp.push(<OwnMesssage key={message.id} message={message} />);
                return temp
              }
    
              if(prevDate && moment(prevDate).calendar() !== moment(currentDate).calendar()){
                temp.push(<Divider key={message.createdAt} date={message.createdAt} />)
              }
              
              temp.push(<Message key={message.id} message={message} />);
              return temp
            })}
    
          </div>
        )
}

export default MessageList;