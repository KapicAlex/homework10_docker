import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import ChatPage from "../../pages/chat/chat";
import LoginPage from "../../pages/login/login";
import MessageEditorPage from "../../pages/messageEditor/messageEditor";
import NotFoundPage from "../../pages/notFound/notFound";
import UserEditorPage from "../../pages/usersEditor/usersEditor";
import UserListPage from "../../pages/usersList/usersList";
import { useSelector } from "react-redux";
import { ROUTER } from "../../enums/routes/routes";
import "./main.css";

const Main = () => {
  const user = useSelector(state => state.profile.user);
  const userIsAdmin = user ? user.role === "admin" : false;

  const PrivateRoute = ({ children, ...rest }) => {
    if (user) return <Route {...rest} />;

    return <Redirect to={ROUTER.login} />;
  };

  const AdminRoute = ({ children, ...rest }) => {
    if (userIsAdmin) return <Route {...rest} />;

    return <Redirect to={ROUTER.chat} />;
  };

  return (
    <main className="main">
      <Switch>
        <Route exact path={ROUTER.homepage}>
          <Redirect to={ROUTER.chat} />
        </Route>
        <Route
          exact
          path={ROUTER.login}
          component={LoginPage}>
          {user && <Redirect to={ROUTER.userList} />}
        </Route>
        <PrivateRoute
          exact
          path={ROUTER.chat}
          component={ChatPage} />
        <PrivateRoute
          path={ROUTER.messageEditor_$ID}
          render={props => <MessageEditorPage {...props} />} />
        <AdminRoute
          exact
          path={ROUTER.userEditor}
          component={UserEditorPage} />
        <AdminRoute
          exact
          path={ROUTER.userList}
          component={UserListPage} />
        <Route
          exact
          path='*'
          component={NotFoundPage} />
      </Switch>
    </main>
  )
}

export default Main;