import React, {useState} from "react";
import "./message.css";

const Message = ({message}) => {
  const {avatar, user, text, createdAt, editedAt} = message;
  const time = editedAt ? new Date(editedAt) : new Date(createdAt);
  const [isLike, setIsLike] = useState(false);

  return (
      <div className="message">
        <img src={avatar} alt="avatar" className="message-user-avatar" />
        <div className="message-main">
          <div className="message-user-name">{user}</div>
          <div className="message-text">{text}</div>
          <div className="message-time">{time.toLocaleTimeString("ru-Ru", {timeStyle: 'short'})}</div>
          <button className={isLike ? "message-liked" : "message-like"} onClick={() => setIsLike(!isLike)}>
            <i className="fas fa-thumbs-up"></i>
          </button>
        </div>
      </div>
    )
}

export default Message;