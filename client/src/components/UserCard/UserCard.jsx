import React from "react";
import "./userCard.css";

const UserCard= ({ user }) => {
  const { username, createdAt, avatar, email } = user;
  const regDate = new Date(createdAt);
  
  return (
    <div className="user-card">
      <div className="avatar">
        <img src={avatar} alt="avatar" />
      </div>
      <div className="user-info">
        <h2 className="username">{username}</h2>
        <div>
          E-mail:&nbsp;
          <span>{email}</span>
        </div>
        <div>
          Registration date:&nbsp;
          <span>{regDate.toLocaleDateString()}</span>
        </div>
      </div>
    </div>
  )
}
export default UserCard;
