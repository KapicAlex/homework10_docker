import { createAsyncThunk, createAction } from "@reduxjs/toolkit";
import { user as userService } from "../../services/services";
import { GET_USERS, ADD_USER, EDIT_USER, DELETE_USER, RESET_ERROR_USERS } from "../actionType/actionTypes";

const getAllUsers = createAsyncThunk(
  GET_USERS,
  async () => await userService.getAllUsers()
);

const addUser = createAsyncThunk(
  ADD_USER,
  async (user) => await userService.addUser(user)
);

const editUser = createAsyncThunk(
  EDIT_USER,
  async (user) => await userService.editUser(user)
);

const deleteUser = createAsyncThunk(
  DELETE_USER,
  async (user) => await userService.deleteUser(user)
);

const resetError = createAction(RESET_ERROR_USERS);


export { getAllUsers, addUser, editUser, deleteUser, resetError };