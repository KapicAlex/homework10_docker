import { createAsyncThunk, createAction } from "@reduxjs/toolkit";
import { message as messageService } from "../../services/services";
import { GET_MESSAGES, ADD_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE, RESET_ERROR_MESSAGES } from "../actionType/actionTypes";

const getAllMessages = createAsyncThunk(
  GET_MESSAGES,
  async () => await messageService.getAllMessages()
);

const addMessage = createAsyncThunk(
  ADD_MESSAGE,
  async (message) => await messageService.addMessage(message)
);

const editMessage = createAsyncThunk(
  EDIT_MESSAGE,
  async (message) => await messageService.editMessage(message)
);

const deleteMessage = createAsyncThunk(
  DELETE_MESSAGE,
  async (id) => await messageService.deleteMessage(id)
);

const resetError = createAction(RESET_ERROR_MESSAGES);


export { getAllMessages, addMessage, editMessage, deleteMessage, resetError };