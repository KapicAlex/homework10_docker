import { createSlice } from "@reduxjs/toolkit";
import { getAllMessages, addMessage, editMessage, deleteMessage, resetError } from "../actions/chat";
import { errorMessages } from "../../enums/error/errors.enum";

const initialState = {
  messages: [],
  preloader: false,
  error: {
    status: false,
    errorMessage: ""
  }
}

const chatSlice = createSlice({
  name: "chat",
  initialState,
  reducers: {},
  extraReducers: {
    [getAllMessages.pending]: (state, action) => {
      state.preloader = true;
      state.error.status = false;
      state.error.errorMessage = "";
    },
    [getAllMessages.fulfilled]: (state, action) => {
      state.preloader = false;
      state.messages = action.payload;
    },
    [getAllMessages.rejected]: (state, action) => {
      state.preloader = false;
      state.error.status = true;
      state.error.errorMessage = errorMessages.FAILED_LOAD_MESSAGES;
    },
    [addMessage.fulfilled]: (state, action) => {
      state.messages.push(action.payload);
    },
    [editMessage.fulfilled]: (state, action) => {
      const message = action.payload;
      state.messages = state.messages.map(oldMessage => {
        if (oldMessage.id === message.id) return message;
        return oldMessage;
      })
    },
    [deleteMessage.fulfilled]: (state, action) => {
      const id = action.payload;
      state.messages = state.messages.filter(message => message.id !== id);
    },
    [resetError]: (state) => {
      state.error.status = false;
      state.error.errorMessage = "";
    }
  },
})

export default chatSlice.reducer;