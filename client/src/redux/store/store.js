import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { chatReducer, profileReducer, usersReducer } from "../reducers/rootReducer";

const store = configureStore({
  reducer: {
    users: usersReducer,
    profile: profileReducer,
    chat: chatReducer,
  },
  middleware: getDefaultMiddleware({ thunk: true }),
});

export default store;