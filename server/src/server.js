import express from 'express';
import routes from "./routes/routes.js";
import cors from 'cors';

const app = express();

app.use(cors());
app.use(express.json());
routes(app);

const PORT = process.env.SERVER_PORT || 3001;

app.listen(PORT, () => {
  console.info(`Server listening on port ${PORT}!`);
});
